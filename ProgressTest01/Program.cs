﻿namespace Exe01
{
    internal class Program
    {
        public static void ConvertFromDecimal(int decimalNumber)
        {
            string binary = Convert.ToString(decimalNumber, 2);
            string octal = Convert.ToString(decimalNumber, 8);
            string hex = Convert.ToString(decimalNumber, 16).ToUpper();

            Console.WriteLine($"Decimal: {decimalNumber}");
            Console.WriteLine($"Binary: {binary}");
            Console.WriteLine($"Octal: {octal}");
            Console.WriteLine($"Hex: {hex}");
        }

        public static void ConvertFromBinary(string binaryNumber)
        {
            int decimalNumber = Convert.ToInt32(binaryNumber, 2);
            string octal = Convert.ToString(decimalNumber, 8);
            string hex = Convert.ToString(decimalNumber, 16).ToUpper();

            Console.WriteLine($"Binary: {binaryNumber}");
            Console.WriteLine($"Decimal: {decimalNumber}");
            Console.WriteLine($"Octal: {octal}");
            Console.WriteLine($"Hex: {hex}");
        }

        public static void ConvertFromHex(string hexNumber)
        {
            int decimalNumber = Convert.ToInt32(hexNumber, 16);
            string binary = Convert.ToString(decimalNumber, 2);
            string octal = Convert.ToString(decimalNumber, 8);

            Console.WriteLine($"Hex: {hexNumber}");
            Console.WriteLine($"Decimal: {decimalNumber}");
            Console.WriteLine($"Binary: {binary}");
            Console.WriteLine($"Octal: {octal}");
        }

        static void Main(string[] args)
        {
            ConvertFromDecimal(2021);
            Console.WriteLine();

            ConvertFromBinary("101111001010111001110");
            Console.WriteLine();

            ConvertFromHex("6DA3F9");
            Console.ReadKey();
        }
    }
}